var express = require('express');
var router = express.Router();
var InfoObject = require('../models/InfoObject');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/news', function(req, res, next){
  InfoObject.find({IsNews: true}, function(err, news){
    if(err) {
      return res.json({'Error': err});
    }

    res.json(news);
  })
});

module.exports = router;
