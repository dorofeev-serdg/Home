var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    Id: { type: Number, required: true },
    DateCreated: { type: Date, required: true },
    DateShowTill: { type: Date, required: true },
    Title: { type: String, required: true },
    Header: { type: String, required: true },
    Body: { type: String, required: true },
    IsActive: { type: Boolean, required: true },
    IsArticle: { type: Boolean, required: true },
    IsNews: { type: Boolean, required: true }
});

module.exports = mongoose.model('InfoObject', schema);