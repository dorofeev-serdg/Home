import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NewsFeedComponent } from './news/newsfeed/newsfeed.component';

import { NewsService } from './services/news/news.service';
import { NewsListItemComponent } from './news/news-list-item/news-list-item.component';
import { MainHeaderComponent } from './main-header/main-header.component';

import { FooterComponent } from './footer/footer.component';
import { InDevelopmentComponent } from './in-development/in-development.component';
import { ErrorPageComponent } from './error-page/error-page.component';

import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { NewsDetailsComponent } from './news/news-details/news-details.component';
import { LOCALE_ID } from '@angular/core';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';

import { HttpModule } from '@angular/http';
import { ArticleListComponent } from './articles/article-list/article-list.component';


@NgModule({
  declarations: [
    AppComponent,
    InDevelopmentComponent,
    HeaderComponent,
    NewsFeedComponent,
    NewsListItemComponent,
    MainHeaderComponent,
    FooterComponent,
    ErrorPageComponent,
    HomeComponent,
    NewsDetailsComponent,
    SignupComponent,
    SigninComponent,
    ArticleListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [
    NewsService
    //,{ provide: LOCALE_ID, useValue: "ru-RU" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
