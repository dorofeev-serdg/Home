import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-in-development',
  templateUrl: './in-development.component.html',
  styleUrls: ['./in-development.component.css']
})
export class InDevelopmentComponent implements OnInit {

  InDevelopmentUrl:string = 'https://goo.gl/rcf7A1';

  constructor() { }

  ngOnInit() {
  }

}
