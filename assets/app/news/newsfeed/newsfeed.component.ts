import { Component, OnInit } from '@angular/core';
import { News } from './../../models/news';
import { NewsService } from './../../services/news/news.service';

@Component({
  selector: 'app-news-feed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css']
})
export class NewsFeedComponent implements OnInit {

  NewsArray: News[];
  constructor(private newsService:NewsService) { }

  ngOnInit() {
    this.onGet();
  }

  onGet(){
    if(this.newsService.getNews().length > 0) {
      this.NewsArray = this.newsService.getNews();
    } else {

      this.newsService.getNewsObservable().subscribe(
        (news: News[]) => {
          this.newsService.setNews(news);
          this.NewsArray = news;
        },
        (error) => {
          console.log(error);
      });
    }
  }
}
