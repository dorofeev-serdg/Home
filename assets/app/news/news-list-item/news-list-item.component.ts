import { Component, OnInit, Input } from '@angular/core';
import { News } from './../../models/news';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-news-list-item',
  templateUrl: './news-list-item.component.html',
  styleUrls: ['./news-list-item.component.css']
})
export class NewsListItemComponent implements OnInit {
  @Input() newsItem: News;

  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.newsItem);
  }
  
}
