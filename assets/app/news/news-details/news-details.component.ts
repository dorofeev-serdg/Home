import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NewsService } from './../../services/news/news.service';
import { News } from './../../models/news';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {

  currentNews:News;

  constructor(private newsService: NewsService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const id = +this.route.snapshot.params['id']; 

    if(this.newsService.getNews().length > 0){
      this.currentNews = this.newsService.getNewsById(id);
    }
    else{
      this.newsService.getNewsObservable().subscribe(
        (news: News[]) => {
          this.newsService.setNews(news);
          this.currentNews = this.newsService.getNewsById(id);
        },
        (error) => {
          console.log(error);
      });      
    }  
  }
}
