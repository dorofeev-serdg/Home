export class News {
    public Id:number;
    public DateCreated:Date;
    public DateShowTill: Date;
    public Title:string;
    public Header:string;
    public Body:string;
    public IsActive:boolean;

    constructor(
        id:number, 
        dateCreated:Date, 
        dateShowTill:Date, 
        title:string, 
        header:string, 
        body:string, 
        isActive:boolean){
            
        this.Id = id;
        this.DateCreated = dateCreated;
        this.DateShowTill = dateShowTill;
        this.Title = title;
        this.Header = header;
        this.Body = body;
        this.IsActive = isActive;
    }
}