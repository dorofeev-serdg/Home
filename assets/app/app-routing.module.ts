import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsFeedComponent } from './news/newsfeed/newsfeed.component';
import { NewsDetailsComponent } from './news/news-details/news-details.component';
import { InDevelopmentComponent } from './in-development/in-development.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'news', component: NewsFeedComponent },
    { path: 'news/:id', component: NewsDetailsComponent },
    { path: 'specific-chat', component: InDevelopmentComponent },
    { path: 'common-chat', component: InDevelopmentComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'signin', component: SigninComponent},
    { path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'}},
    { path: '**', redirectTo: '/not-found' }
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {
}
