import { Injectable } from '@angular/core';
import { News } from './../../models/news';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class NewsService {

  private newsArray:News[] = new Array<News>();
  constructor(private http:Http) { }

  compareNews(a,b) {
    if (a.Id < b.Id)
      return -1;
    if (a.Id > b.Id)
      return 1;
    return 0;
  }

  setNews(news: News[]) {
    this.newsArray = news;
  }

  getNews(){
    return this.newsArray.slice();
  }

  getNewsObservable() {
    return this.http.get('/api/news')
      .map( (responce: Response) => {
          const data = responce.json();

          let newsArray = new Array<News>();
          data.forEach(element => {
            newsArray.push( new News( 
              element.Id, 
              element.DateCreated, 
              element.DateShowTill, 
              element.Title, 
              element.Header, 
              element.Body, 
              element.IsActive) );
          });
    
          newsArray = newsArray.sort(function(a,b) {return (a.Id > b.Id) ? 1 : ((b.Id > a.Id) ? -1 : 0);});
          newsArray = newsArray.reverse();

          return newsArray;
      })
      .catch(
          (error: Response) => {
              console.log(error);
              return Observable.throw('Something went wrong on getting servers');
          }
      )
  }

  getNewsById(id:number):News {
    let requiredNews = this.newsArray.find(element => {return element.Id == id});
    return Object.assign({}, requiredNews);
  }

}
